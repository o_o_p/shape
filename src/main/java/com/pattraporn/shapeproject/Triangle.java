/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.shapeproject;

/**
 *
 * @author Pattrapon N
 */
public class Triangle {
    double t;
    double h;
    static final double g =0.5;
    public Triangle(double t,double h){
        this.t = t;
        this.h = h;
    }
    public double TriangleArea(){
        return  g * t * h;
    }
    public double getT(){
        return t;
    }
     public double getH(){
        return h;
    }
      public void setR(double t,double h){
       if(t<=0 &&h <=0){
           System.out.println("Error: Radius must more than zero!!");
            return;}
        this.t = t;
        this.h = h;
           
       }
    }

