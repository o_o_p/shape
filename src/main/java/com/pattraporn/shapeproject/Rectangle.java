/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.shapeproject;

/**
 *
 * @author Pattrapon N
 */
public class Rectangle {
    double w;
    double l;
    public Rectangle(double w,double l){
        this.w = w;
        this.l = l;
    }
    public double RectangleArea(){
        return  w*l;
    }
    public double getR(){
        return w;
       
    }
    public double getL(){
        return l;
       
    }
     public void setR(double w,double l){
       if(w<=0 && l <=0){
           System.out.println("Error: Radius must more than zero!!");
            return;}
        this.w = w;
        this.l = l;
           
       }
    }
     
     

