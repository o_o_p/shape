/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.shapeproject;

/**
 *
 * @author Pattrapon N
 */
public class Square {
    double s ;
    public Square(double s){
        this.s = s;
    }
    public double SquareArea(){
        return  s*s;
    }
    public double getR(){
        return s;
    }
    public void setR(double s){
        if(s<=0){
            System.out.println("Error: Radius must more than zero!!");
            return;}
        this.s=s;
    }
}
